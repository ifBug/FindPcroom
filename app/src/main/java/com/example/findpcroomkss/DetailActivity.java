package com.example.findpcroomkss;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.demono.AutoScrollViewPager;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nhn.android.maps.NMapController;
import com.nhn.android.maps.NMapOverlay;
import com.nhn.android.maps.NMapOverlayItem;
import com.nhn.android.maps.NMapView;
import com.nhn.android.maps.maplib.NGeoPoint;
import com.nhn.android.maps.overlay.NMapPOIdata;
import com.nhn.android.maps.overlay.NMapPOIitem;
import com.nhn.android.mapviewer.overlay.NMapCalloutCustomOverlay;
import com.nhn.android.mapviewer.overlay.NMapCalloutOverlay;
import com.nhn.android.mapviewer.overlay.NMapOverlayManager;
import com.nhn.android.mapviewer.overlay.NMapPOIdataOverlay;

import java.util.ArrayList;

import Adapter.DetailViewPagerAdapter;
import Data.PcroomData;
import Dialog.ReservationDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import mapviewer.NMapCalloutCustomOldOverlay;
import mapviewer.NMapCalloutCustomOverlayView;
import mapviewer.NMapPOIflagType;
import mapviewer.NMapViewerResourceProvider;

import static com.example.findpcroomkss.BuildConfig.DEBUG;

public class DetailActivity extends AppCompatActivity {

    DatabaseReference mDatabase;

    private NMapViewerResourceProvider mMapViewerResourceProvider;
    private NMapOverlayManager mOverlayManager;
    private NMapController mMapController;

    @BindView(R.id.detail_scroll_view)
    NestedScrollView scrollView;
    @OnTouch(R.id.mapView)  //스크롤 뷰가 지도상을 터치할땐 움직이지 않게 만들기
    public boolean OnTouch (View v) {
        if(v.equals(mMapView)) {
            scrollView.requestDisallowInterceptTouchEvent(true);
        }
        return false;
    }

    private final NMapPOIdataOverlay.OnStateChangeListener onPOIdataStateChangeListener = new NMapPOIdataOverlay.OnStateChangeListener() {

        @Override
        public void onCalloutClick(NMapPOIdataOverlay poiDataOverlay, NMapPOIitem item) {
            if (DEBUG) {
//                Log.i(LOG_TAG, "onCalloutClick: title=" + item.getTitle());
            }

            // [[TEMP]] handle a click event of the callout
//            Toast.makeText(NMapViewer.this, "onCalloutClick: " + item.getTitle(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onFocusChanged(NMapPOIdataOverlay poiDataOverlay, NMapPOIitem item) {
            if (DEBUG) {
                if (item != null) {
//                    Log.i(LOG_TAG, "onFocusChanged: " + item.toString());
                } else {
//                    Log.i(LOG_TAG, "onFocusChanged: ");
                }
            }
        }
    };

    private final NMapOverlayManager.OnCalloutOverlayViewListener onCalloutOverlayViewListener = new NMapOverlayManager.OnCalloutOverlayViewListener() {

        @Override
        public View onCreateCalloutOverlayView(NMapOverlay itemOverlay, NMapOverlayItem overlayItem, Rect itemBounds) {

            if (overlayItem != null) {
                // [TEST] 말풍선 오버레이를 뷰로 설정함
                String title = overlayItem.getTitle();
                if (title != null && title.length() > 5) {
                    return new NMapCalloutCustomOverlayView(DetailActivity.this, itemOverlay, overlayItem, itemBounds);
                }
            }

            // null을 반환하면 말풍선 오버레이를 표시하지 않음
            return null;
        }

    };

    private final NMapOverlayManager.OnCalloutOverlayListener onCalloutOverlayListener = new NMapOverlayManager.OnCalloutOverlayListener() {

        @Override
        public NMapCalloutOverlay onCreateCalloutOverlay(NMapOverlay itemOverlay, NMapOverlayItem overlayItem,
                                                         Rect itemBounds) {

            // handle overlapped items
            if (itemOverlay instanceof NMapPOIdataOverlay) {
                NMapPOIdataOverlay poiDataOverlay = (NMapPOIdataOverlay)itemOverlay;

                // check if it is selected by touch event
                if (!poiDataOverlay.isFocusedBySelectItem()) {
                    int countOfOverlappedItems = 1;

                    NMapPOIdata poiData = poiDataOverlay.getPOIdata();
                    for (int i = 0; i < poiData.count(); i++) {
                        NMapPOIitem poiItem = poiData.getPOIitem(i);

                        // skip selected item
                        if (poiItem == overlayItem) {
                            continue;
                        }

                        // check if overlapped or not
                        if (Rect.intersects(poiItem.getBoundsInScreen(), overlayItem.getBoundsInScreen())) {
                            countOfOverlappedItems++;
                        }
                    }

                    if (countOfOverlappedItems > 1) {
                        String text = countOfOverlappedItems + " overlapped items for " + overlayItem.getTitle();
                        Toast.makeText(DetailActivity.this, text, Toast.LENGTH_LONG).show();
                        return null;
                    }
                }
            }

            // use custom old callout overlay
            if (overlayItem instanceof NMapPOIitem) {
                NMapPOIitem poiItem = (NMapPOIitem)overlayItem;

                if (poiItem.showRightButton()) {
                    return new NMapCalloutCustomOldOverlay(itemOverlay, overlayItem, itemBounds,
                            mMapViewerResourceProvider);
                }
            }

            // use custom callout overlay
            return new NMapCalloutCustomOverlay(itemOverlay, overlayItem, itemBounds, mMapViewerResourceProvider);

            // set basic callout overlay
            //return new NMapCalloutBasicOverlay(itemOverlay, overlayItem, itemBounds);
        }

    };


    //    ArrayList<PcroomData> datas = new ArrayList<>();
//  int position = 0;
    PcroomData data;
    String key;
    int count = 0;

//    ArrayList<String> imageUrl_1 = new ArrayList<>();

    @BindView(R.id.mapView)
    NMapView mMapView;      //mapview 버터나이프

    @BindView(R.id.detail_asvp)
    AutoScrollViewPager viewPager;
    DetailViewPagerAdapter vpadApter;

    @BindView(R.id.detail_iv_back)
    ImageView back;
// 수정
    @BindView(R.id.detail_title)
    TextView title;

    @BindView(R.id.item_tv_available_parking)
    TextView parkinglot;

    @BindView(R.id.item_tv_available_smokingArea)
    TextView smokingArea;

    @BindView(R.id.item_tv_available_word)
    TextView word;

    @BindView(R.id.item_tv_available_printerScanner)
    TextView printerScanner;

//  수정

    @BindView(R.id.detail_ll_reservation)
    LinearLayout reservation;

    @OnClick({R.id.detail_ll_reservation})
    public void OnClickReservation(View v) {
        if(v.equals(reservation)) {
            AlertDialog.Builder ad = new AlertDialog.Builder(this);
            ad.setTitle("알림");
            ad.setMessage("예약하시겠습니까?")
                    .setCancelable(false)
                    .setPositiveButton("예약 합니다", new DialogInterface.OnClickListener() {
                        @Override
                        //확인을 누르면 하는 행동
                                public void onClick(DialogInterface dialogInterface, int i) {
//                            Toast.makeText(DetailActivity.this, "확인", Toast.LENGTH_SHORT).show();
                            ReservationDialog reservationDialog = new ReservationDialog(DetailActivity.this, count);
                            reservationDialog.show();
                        }
                    })
                    .setNegativeButton("예약 하지 않습니다", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Toast.makeText(DetailActivity.this, "취소", Toast.LENGTH_SHORT).show();
                        }
                    }).setNeutralButton("닫기", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Toast.makeText(DetailActivity.this, "닫기", Toast.LENGTH_SHORT).show();
                }
            });

            AlertDialog dialog = ad.create();
            dialog.show();
        }
    }

    @OnClick(R.id.detail_iv_back)
    public void OnBack(View v) {
        if (v.equals(back)) {
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        key = getIntent().getStringExtra("key");

        try {
            Log.e("key",key);

            mDatabase = FirebaseDatabase.getInstance().getReference();
            mDatabase.child("pcbang").child(key).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    data = dataSnapshot.getValue(PcroomData.class);
                    if (data == null) {
                        Toast.makeText(DetailActivity.this, "잘못된 접근입니다.", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        setVp();
                        initMap();
                        setView();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Toast.makeText(DetailActivity.this, "잘못된 접근입니다.", Toast.LENGTH_SHORT).show();
                    finish();
                }
            });
        } catch (Exception e) {
            Toast.makeText(DetailActivity.this, "잘못된 접근입니다.", Toast.LENGTH_SHORT).show();
            finish();
        }

//        Toast.makeText(this, position+"", Toast.LENGTH_SHORT).show();

//      makeDummy();
        //타이틀
        //textview 4개(주차 가능/흡연실 있음/워드 가능/프린터, 스캐너, 프린터 스캐너)

    }

    private void initMap() {
        mMapView.setClickable(true);
        mMapView.setEnabled(true);
        mMapView.setFocusable(true);
        mMapView.setFocusableInTouchMode(true);
        mMapView.requestFocus();

        // create overlay manager
        mOverlayManager = new NMapOverlayManager(this, mMapView, mMapViewerResourceProvider);
        // register callout overlay listener to customize it.
        mOverlayManager.setOnCalloutOverlayListener(onCalloutOverlayListener);
        // register callout overlay view listener to customize it.
        mOverlayManager.setOnCalloutOverlayViewListener(onCalloutOverlayViewListener);

        // create resource provider
        mMapViewerResourceProvider = new NMapViewerResourceProvider(this);

        // use map controller to zoom in/out, pan and set map center, zoom level etc.
        mMapController = mMapView.getMapController();

        mMapController.setMapCenter(new NGeoPoint(data.getLongitude(), data.getLatitude()), 12);
        setPOIdataOverlay();
    }

    private void setPOIdataOverlay() {

        // Markers for POI item
        int markerId = NMapPOIflagType.PIN;

        // set POI data
        NMapPOIdata poiData = new NMapPOIdata(2, mMapViewerResourceProvider);
        poiData.beginPOIdata(2);
        NMapPOIitem item = poiData.addPOIitem(data.getLongitude(), data.getLatitude(), data.getName(), markerId, 0);
        item.setRightAccessory(true, NMapPOIflagType.CLICKABLE_ARROW);
        poiData.addPOIitem(127.061, 37.51, "Pizza 123-456", markerId, 0);
        poiData.endPOIdata();

        // create POI data overlay
        NMapPOIdataOverlay poiDataOverlay = mOverlayManager.createPOIdataOverlay(poiData, null);

        // set event listener to the overlay
        poiDataOverlay.setOnStateChangeListener(onPOIdataStateChangeListener);

        // select an item
        poiDataOverlay.selectPOIitem(0, true);

        // show all POI data
        //poiDataOverlay.showAllPOIdata(0);
    }

    private void setView() {
        title.setText(data.getName());

        if(data.isBool_parkinglot()) {
            parkinglot.setText("주차가능");
            parkinglot.setBackgroundColor(Color.parseColor("#E91E63"));
            parkinglot.setTextColor(Color.parseColor("#ffffff"));
        } else {
            parkinglot.setText("주차불가");
            parkinglot.setBackgroundColor(Color.parseColor("#aaaaaa"));
        }

        if(data.isBool_smokingArea()) {
            smokingArea.setText("흡연실있음");
            smokingArea.setBackgroundColor(Color.parseColor("#E91E63"));
            smokingArea.setTextColor(Color.parseColor("#ffffff"));
        } else {
            smokingArea.setText("흡연실없음");
            smokingArea.setBackgroundColor(Color.parseColor("#aaaaaa"));
        }
        if(data.isBool_word()) {
            word.setText("문서작업가능");
            word.setBackgroundColor(Color.parseColor("#E91E63"));
            word.setTextColor(Color.parseColor("#ffffff"));
        } else {
            word.setText("문서작업불가");
            word.setBackgroundColor(Color.parseColor("#aaaaaa"));
        }

        if(data.isBool_print() && data.isBool_scanner()) {
            printerScanner.setText("프린터 스캐너");
            printerScanner.setBackgroundColor(Color.parseColor("#E91E63"));
            printerScanner.setTextColor(Color.parseColor("#ffffff"));
        } else if(data.isBool_print() && !data.isBool_scanner()) {
            printerScanner.setText("프린터");
            printerScanner.setBackgroundColor(Color.parseColor("#E91E63"));
            printerScanner.setTextColor(Color.parseColor("#ffffff"));
        } else if(!data.isBool_print() && data.isBool_scanner()) {
            printerScanner.setText("스캐너");
            printerScanner.setBackgroundColor(Color.parseColor("#E91E63"));
            printerScanner.setTextColor(Color.parseColor("#ffffff"));
        } else {
            printerScanner.setText("프린터 스캐너");
            printerScanner.setBackgroundColor(Color.parseColor("#aaaaaa"));
        }
    }

    public void SetReservation(int count) {
        this.count = count;
        Toast.makeText(this, "reservatoin "+ count, Toast.LENGTH_SHORT).show();
    }

    private void setVp() {
        vpadApter = new DetailViewPagerAdapter(this, data.getImageUrlList());
        viewPager.setAdapter(vpadApter);
        viewPager.startAutoScroll();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

//    private void makeDummy() {
//        datas.add(new PcroomData("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTJ0iwzvFYgtv7pJsV1tmnMnRYvY9D12b4tRVh9ULWTygZ2tmWZ",1.2f,"헤라PC","2", "시간당 1200원 (회원가)", "CPU I7-7800K", "GTX 1060"));
//
//        ArrayList<String> payPolicy_1 = new ArrayList<>();
//        payPolicy_1.add("card");
//        payPolicy_1.add("bitcoin");
//        payPolicy_1.add("Samsung Pay");
//
//        imageUrl_1.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZ5gGtKCmBANlYAL-nVjHBXcNRWJWtoeAycv8mkN-N_Rc0dddoAw");
//        imageUrl_1.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRIbCcoUlm2VgtvcKW5ftjFKHCJeN7js0XGMiaGW9ZBNctpwmxGOA");
//        imageUrl_1.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT3gkaAuonVGHS4Q1vKac5x6In8D958G4RS_WKSdnL9mS9O-bE_sg");
//
//        ArrayList<String> payPolicy_2 = new ArrayList<>();
//        payPolicy_2.add("card");
//        payPolicy_2.add("Kakao pay");
//        payPolicy_2.add("LG Pay");
//
//        ArrayList<String> imageUrl_2 = new ArrayList<>();
//        imageUrl_2.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSZ1ZYHzM3BwfmZJQTTofXmduXY04uJ7vLUH_FtbjhvSVU9g5Cw");
//        imageUrl_2.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS0hUC6T7AA4L-6YwifJ7xnNNNqWDNVXP7CKZfCOBp40RRUu-Y7Qw");
//        imageUrl_2.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT3gkaAuonVGHS4Q1vKac5x6In8D958G4RS_WKSdnL9mS9O-bE_sg");
//
//
//
//        datas.add(new PcroomData( "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTJ0iwzvFYgtv7pJsV1tmnMnRYvY9D12b4tRVh9ULWTygZ2tmWZ", 1.2f, "제노PC", "2", "시간당 900원", "i5-7600u", "GTX 1050", "16GB", true, "LG big monitor", "logitec G39", "logitec B170", "USB C-type", payPolicy_1, imageUrl_1, "서울특별시 용산구 24길 5로", "서울특별시 용산구 보광동 259-1", 312.5, 223.6, true, true, true, true, true, true, "장당 150원", "장당 300원", false, "스캐너가 없음"));
//        datas.add(new PcroomData( "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQtwByDCppuRRqmEGVHkxveAX7rNbXl5BL4DcCKP35YBCqTWp0gPA", 1.2f, "집앞PC", "2", "시간당 900원", "i5-7600u", "GTX 1050", "16GB", true, "LG big monitor", "logitec G39", "logitec B170", "USB C-type", payPolicy_2, imageUrl_2, "서울특별시 용산구 24길 5로", "서울특별시 용산구 보광동 259-1", 312.5, 223.6, true, true, false, true, true, true, "장당 150원", "장당 300원", false, "스캐너가 없음"));
//
//    }
}

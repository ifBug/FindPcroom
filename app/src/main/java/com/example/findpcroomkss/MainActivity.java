package com.example.findpcroomkss;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Set;

import Adapter.MainRecyclerviewAdapter;
import Data.PcroomData;
import Util.SqlHelper;
import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class

MainActivity extends AppCompatActivity {



    SqlHelper helper;
    SQLiteDatabase db;
    String dbName = "pcroom.db";
    String tableName = "PCROOM";

    @BindView(R.id.main_iv_map)
    ImageView map;
    @OnClick (R.id.main_iv_map)
    public void OnClickSettingMap(View v) {
        if(v.equals(map)) {
            Intent intent = new Intent(this, NMapViewer.class);
            startActivity(intent);
            finish();
        }
    }

    @BindView(R.id.main_iv_setting)
    ImageView setting;
    @OnClick (R.id.main_iv_setting)
    public void OnClickSetting(View v) {
        if(v.equals(setting)) {
            //이너 클래스에서만 인텐트 받아들일떄 this 가 안된다.
            Intent intent = new Intent(this, SettingActivity.class);
            startActivity(intent);
            finish();
        }
    }
    @BindView(R.id.main_rv)
    RecyclerView rv;
    MainRecyclerviewAdapter adapter;
    ArrayList<PcroomData> datas = new ArrayList<>();

    private DatabaseReference mDatabase;    //데이터 베이스 선언

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        helper = new SqlHelper(this, dbName, null,2);
        db = helper.getWritableDatabase();

//        makeDummy();
//        makeDatabase();

        loadData();

    }

    private void loadData() {

        final ProgressDialog dialog = ProgressDialog.show(MainActivity.this, "","데이터를 불러오는 중입니다.");

        mDatabase.child("pcbang").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot ds : dataSnapshot.getChildren()) {
                    //피시방에서 불러오는 데이터가 여러개일때
                    datas.add(ds.getValue(PcroomData.class));   //pcbang 에 있는 데이터 불러온다.
                }
                setRv();
                dialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                dialog.dismiss();
            }
        });
    }

    private void makeDatabase() {

        for(PcroomData data : datas) {
            String key = mDatabase.child("pcbang").push().getKey();
            data.setKey(key);
            mDatabase.child("pcbang").child(key).setValue(data);
        }

//
//        int count = 0;
//        try {
//            db = openOrCreateDatabase(dbName, Context.MODE_PRIVATE,null);
//            String sql = " SELECT * FROM " + tableName + ";";
//            Cursor cursor = db.rawQuery(sql, null);
//            cursor.moveToFirst();
//            while(!cursor.isAfterLast()) {
//                count++;
//                cursor.moveToNext();
//            }
//        } catch (Exception e) {
//        }
//
//        if(count == 0) {
//            try {
//                for(PcroomData data : datas) {
//                    String sql = " INSERT INTO " + tableName + " VALUES(" + data.toInsertSqlValues() + " );";
//
//                    db.execSQL(sql);
//                }
//            } catch (Exception e) {
//
//            }
//        } else {
//            return;
//        }
//
//        try {
//            String sql = " SELECT * FROM " + tableName + ";";
//            Cursor cursor = db.rawQuery(sql, null);
//            cursor.moveToFirst();
//            while(!cursor.isAfterLast()) {
//                Toast.makeText(this, cursor.getString((helper.ADDRESS)), Toast.LENGTH_LONG).show();
//            }
//        } catch (Exception e) {
//            Log.e("into", e.getMessage().toString());
//        }
    }

    private void setRv() {
        RecyclerView.LayoutManager lm = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        rv.setLayoutManager(lm);
        adapter = new MainRecyclerviewAdapter(this, datas);
        rv.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

//    private void makeDummy() {
//        datas.add(new PcroomData("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTJ0iwzvFYgtv7pJsV1tmnMnRYvY9D12b4tRVh9ULWTygZ2tmWZ",1.2f,"헤라PC","2", "시간당 1200원 (회원가)", "CPU I7-7800K", "GTX 1060"));
//
//        ArrayList<String> payPolicy_1 = new ArrayList<>();
//        payPolicy_1.add("card");
//        payPolicy_1.add("bitcoin");
//        payPolicy_1.add("Samsung Pay");
//
//        ArrayList<String> imageUrl_1 = new ArrayList<>();
//        imageUrl_1.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZ5gGtKCmBANlYAL-nVjHBXcNRWJWtoeAycv8mkN-N_Rc0dddoAw");
//        imageUrl_1.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRIbCcoUlm2VgtvcKW5ftjFKHCJeN7js0XGMiaGW9ZBNctpwmxGOA");
//        imageUrl_1.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT3gkaAuonVGHS4Q1vKac5x6In8D958G4RS_WKSdnL9mS9O-bE_sg");
//
//        ArrayList<String> payPolicy_2 = new ArrayList<>();
//        payPolicy_2.add("card");
//        payPolicy_2.add("Kakao pay");
//        payPolicy_2.add("LG Pay");
//
//        ArrayList<String> imageUrl_2 = new ArrayList<>();
//        imageUrl_2.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSZ1ZYHzM3BwfmZJQTTofXmduXY04uJ7vLUH_FtbjhvSVU9g5Cw");
//        imageUrl_2.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS0hUC6T7AA4L-6YwifJ7xnNNNqWDNVXP7CKZfCOBp40RRUu-Y7Qw");
//        imageUrl_2.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT3gkaAuonVGHS4Q1vKac5x6In8D958G4RS_WKSdnL9mS9O-bE_sg");
//
//
//
//        datas.add(new PcroomData( "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTJ0iwzvFYgtv7pJsV1tmnMnRYvY9D12b4tRVh9ULWTygZ2tmWZ", 1.2f, "제노PC", "2", "시간당 900원", "i5-7600u", "GTX 1050", "16GB", true, "LG big monitor", "logitec G39", "logitec B170", "USB C-type", payPolicy_1, imageUrl_1, "서울특별시 용산구 24길 5로", "서울특별시 용산구 보광동 259-1", 312.5, 223.6, true, true, false, true, true, true, "장당 150원", "장당 300원", false, "스캐너가 없음"));
//        datas.add(new PcroomData( "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQtwByDCppuRRqmEGVHkxveAX7rNbXl5BL4DcCKP35YBCqTWp0gPA", 1.2f, "집앞PC", "2", "시간당 900원", "i5-7600u", "GTX 1050", "16GB", true, "LG big monitor", "logitec G39", "logitec B170", "USB C-type", payPolicy_2, imageUrl_2, "서울특별시 용산구 24길 5로", "서울특별시 용산구 보광동 259-1", 312.5, 223.6, true, true, false, true, true, true, "장당 150원", "장당 300원", false, "스캐너가 없음"));
//
//    }
}
package com.example.findpcroomkss;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingActivity extends AppCompatActivity {

    @BindView(R.id.setting_ll_login)
    LinearLayout login_out;
    @OnClick(R.id.setting_ll_login)
    public void OnClickLogin(View v)  {
        if(v.equals(login_out)) {
            //이너 클래스에서만 인텐트 받아들일떄 this 가 안된다.
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);
    }
}

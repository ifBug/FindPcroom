package Data;

import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

public class PcroomData {
    String gu = "";
    String key;
    String imageUrl = "";
    float distance;
    String name = "";
    String reservationAvailable;
    String price;
    String cpu;
    String gpu;
    String ram;
    boolean bool_ssd;
    String monitor;
    String keyboard;
    String mouse;
    String phone_charge;
    ArrayList<String> payPolicy = new ArrayList<>();

    ArrayList<String> imageUrlList = new ArrayList<>();
    String address;
    String oldAddress;
    double longitude;
    double latitude;
    boolean bool_smokingArea;
    boolean bool_card;
    boolean bool_parkinglot;
    boolean bool_word;
    boolean bool_office;
    boolean bool_print;
    String printPrice;
    boolean bool_scanner;


    public String toInsertSqlValues() {
        String result = "";
        result += "null, ";
        result += "'" + name + "', ";
        result += "'" + address + "', ";
        result += "'" + oldAddress + "', ";
        result += "'" + longitude + "', ";

        result += "'" + latitude + "', ";
        result += "'" + cpu + "', ";
        result += "'" + gpu + "', ";
        result += "'" + ram + "', ";
        result += "'" + monitor + "', ";

        result += "'" + keyboard + "', ";
        result += "'" + mouse + "', ";
        result += "'" + phone_charge + "', ";
        result += (bool_smokingArea ? 1 : 0) + ", ";  // 1이나 0이 된다.
        result += (bool_card ? 1 : 0) + ", ";

        result += (bool_parkinglot ? 1 : 0) + ", ";
        result += (bool_office ? 1 : 0) + ", ";
        result += (bool_print ? 1 : 0) + ", ";
        result += (bool_scanner ? 1 : 0) + ", ";
        result += "'" + printPrice + "', ";


        return result;
    }

    public PcroomData(String imageUrl, float distance, String name, String reservationAvailable, String price, String cpu, String gpu) {
        this.imageUrl = imageUrl;
        this.distance = distance;
        this.name = name;
        this.reservationAvailable = reservationAvailable;
        this.price = price;
        this.cpu = cpu;
        this.gpu = gpu;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    public boolean isBool_ssd() {
        return bool_ssd;
    }

    public void setBool_ssd(boolean bool_ssd) {
        this.bool_ssd = bool_ssd;
    }

    public String getMonitor() {
        return monitor;
    }

    public void setMonitor(String monitor) {
        this.monitor = monitor;
    }

    public String getKeyboard() {
        return keyboard;
    }

    public void setKeyboard(String keyboard) {
        this.keyboard = keyboard;
    }

    public String getMouse() {
        return mouse;
    }

    public void setMouse(String mouse) {
        this.mouse = mouse;
    }

    public String getPhone_charge() {
        return phone_charge;
    }

    public void setPhone_charge(String phone_charge) {
        this.phone_charge = phone_charge;
    }

    public ArrayList<String> getPayPolicy() {
        return payPolicy;
    }

    public void setPayPolicy(ArrayList<String> payPolicy) {
        this.payPolicy = payPolicy;
    }

    public ArrayList<String> getImageUrlList() {
        return imageUrlList;
    }

    public void setImageUrlList(ArrayList<String> imageUrlList) {
        this.imageUrlList = imageUrlList;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOldAddress() {
        return oldAddress;
    }

    public void setOldAddress(String oldAddress) {
        this.oldAddress = oldAddress;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public boolean isBool_smokingArea() {
        return bool_smokingArea;
    }

    public void setBool_smokingArea(boolean bool_smokingArea) {
        this.bool_smokingArea = bool_smokingArea;
    }

    public boolean isBool_card() {
        return bool_card;
    }

    public void setBool_card(boolean bool_card) {
        this.bool_card = bool_card;
    }

    public boolean isBool_parkinglot() {
        return bool_parkinglot;
    }

    public void setBool_parkinglot(boolean bool_parkinglot) {
        this.bool_parkinglot = bool_parkinglot;
    }

    public boolean isBool_word() {
        return bool_word;
    }

    public void setBool_word(boolean bool_word) {
        this.bool_word = bool_word;
    }

    public boolean isBool_office() {
        return bool_office;
    }

    public void setBool_office(boolean bool_office) {
        this.bool_office = bool_office;
    }

    public boolean isBool_print() {
        return bool_print;
    }

    public void setBool_print(boolean bool_print) {
        this.bool_print = bool_print;
    }

    public String getPrintPrice() {
        return printPrice;
    }

    public void setPrintPrice(String printPrice) {
        this.printPrice = printPrice;
    }

    public boolean isBool_scanner() {
        return bool_scanner;
    }

    public void setBool_scanner(boolean bool_scanner) {
        this.bool_scanner = bool_scanner;
    }


    public PcroomData() {

    }

    public String getGu() {
        return gu;
    }

    public void setGu(String gu) {
        this.gu = gu;
    }

    public PcroomData(String gu, String imageUrl, String name, String reservationAvailable, String price, String cpu, String gpu, String ram, boolean bool_ssd, String monitor, String keyboard, String mouse, String phone_charge, List<String> payPolicy, float distance, List<String> imageUrlList, String address, String oldAddress, double longitude, double latitude, boolean bool_smokingArea, boolean bool_card, boolean bool_parkinglot, boolean bool_word,  boolean bool_print, String printPrice, boolean bool_scanner) {
        this.gu = gu;
        this.imageUrl = imageUrl;
        this.name = name;
        this.reservationAvailable = reservationAvailable;
        this.price = price;
        this.cpu = cpu;
        this.gpu = gpu;
        this.ram = ram;
        this.bool_ssd = bool_ssd;
        this.monitor = monitor;
        this.keyboard = keyboard;
        this.mouse = mouse;
        this.phone_charge = phone_charge;
        this.payPolicy.addAll(payPolicy);
        this.distance = distance;
        this.imageUrlList.addAll(imageUrlList);
        this.address = address;
        this.oldAddress = oldAddress;
        this.longitude = longitude;
        this.latitude = latitude;
        this.bool_smokingArea = bool_smokingArea;
        this.bool_card = bool_card;
        this.bool_parkinglot = bool_parkinglot;
        this.bool_word = bool_word;
        this.bool_print = bool_print;
        this.printPrice = printPrice;
        this.bool_scanner = bool_scanner;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReservationAvailable() {
        return reservationAvailable;
    }

    public void setReservationAvailable(String reservationAvailable) {
        this.reservationAvailable = reservationAvailable;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public String getGpu() {
        return gpu;
    }

    public void setGpu(String gpu) {
        this.gpu = gpu;
    }
}

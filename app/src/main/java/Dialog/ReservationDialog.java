package Dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.findpcroomkss.DetailActivity;
import com.example.findpcroomkss.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class ReservationDialog extends Dialog {

    Context context;

    @BindView(R.id.dialog_reservation_count)
    EditText count;
    @BindView(R.id.dialog_reservation_state)
    TextView state;
    boolean boolState = false;

    @BindView(R.id.dialog_reservation_check)
    CheckBox check;
    boolean boolCheck = false;

    @BindView(R.id.dialog_reservation_cancel)
    TextView cancel;

    @BindView(R.id.dialog_reservation_ok)
    TextView ok;

    @OnTextChanged(value = R.id.dialog_reservation_count)
    public void OnCountChanged(CharSequence text) {
        try {
            int icount = Integer.parseInt(text.toString());
            state.setText("");
            if(icount < 1 || icount > 8) {
                state.setText("예약인원은 1명이상 8명이하 입니다.");
                boolState = false;
            } else {
                boolState = true;
            }
            checkButton();
        } catch (Exception e) {
            state.setText("숫자만 입력해주세요.");
            boolState = false;
            checkButton();
        }
    }

    @OnClick({R.id.dialog_reservation_ok, R.id.dialog_reservation_cancel})
    public void OnClick(View v) {
        if(v.equals(ok)) {
            ((DetailActivity)context).SetReservation((Integer.parseInt(count.getText().toString())));
            this.dismiss();
        } else if(v.equals(cancel)) {
            ((DetailActivity)context).SetReservation(0);
            this.dismiss();
        }
    }

    private void checkButton() {
        if(boolState && boolCheck) {
            ok.setTextColor(context.getResources().getColor(R.color.colorAccent));
            cancel.setTextColor(context.getResources().getColor(R.color.colorAccent));

            ok.setEnabled(true);
            cancel.setEnabled(true);
        } else {
            ok.setTextColor(context.getResources().getColor(R.color.colorAccent));
            cancel.setTextColor(context.getResources().getColor(R.color.colorAccent));

            ok.setEnabled(false);
        }
    }

    @OnCheckedChanged (R.id.dialog_reservation_check)
    public void OnCheck(CompoundButton button, boolean checked) {
        if(button.equals(check)) {
            if(checked) {
                boolCheck = true;
                checkButton();
            } else {
                boolCheck = false;
                checkButton();
            }
        }
    }


    public ReservationDialog(@NonNull Context context, int icount) {
        super(context);
        this.context = context;
        setContentView(R.layout.dialog_reservation);
        ButterKnife.bind(this);

        count.setText(icount+"");
        if(icount != 0) {
            boolState = true;
            boolCheck = true;

            state.setText("");
            check.setChecked(true);
        }
        checkButton();
    }
}

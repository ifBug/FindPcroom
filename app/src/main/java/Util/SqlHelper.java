package Util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.findpcroomkss.R;

import java.util.ArrayList;
import java.util.Arrays;

public class SqlHelper extends SQLiteOpenHelper {


    public final int ID = 0;
    public final int NAME = 1;
    public final int ADDRESS = 2;
    public final int OLD_ADDRESS = 3;
    public final int LONGITUTDE = 4;

    public final int LATITUDE = 5;
    public final int CPU = 6;
    public final int GPU = 7;
    public final int RAM = 8;
    public final int MONITOR = 9;

    public final int KEYBOARD = 10;
    public final int MOUSE = 11;
    public final int PHONE_CHARGE = 12;
    public final int BOOL_SMOKINGAREA = 13;
    public final int BOOL_CARD = 14;

    public final int BOOL_PARKINGLOT = 15;
    public final int BOOL_OFFICE = 16;
    public final int BOOL_PRINT = 17;
    public final int BOOL_SCANNER = 18;
    public final int PRINTPRICE = 19;

    public final int COLORPRINTPRICE = 20;
    public final int SCANNERPRICE = 21;

    Context context;
    ArrayList<String> values = new ArrayList<>();
    ArrayList<String> types = new ArrayList<>();

    public SqlHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        this.context = context;

        //array를 리스트로 리스트를 어레이 리스트로 바꾼다.
        values = new ArrayList<>(Arrays.asList(context.getResources().getStringArray(R.array.sql_value)));
        types = new ArrayList<>(Arrays.asList(context.getResources().getStringArray(R.array.sql_type)));
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //처음에 데이터베이스 들어왔을때 하는 행동
        String sql = " CREATE TABLE PCROOM (";

        for(int i = 0; i < values.size(); i++) {
            sql += " " + values.get(i) + " " + types.get(i) + ", ";
        }
        sql = sql.substring(0, sql.length()-2);
        sql += ");";
        Log.e("sql",sql);

//                + " (id integer primary key autoincrement, "
//                + " name text, address text, oldAddress text, longitude text, latitude text, cpu text, gpu text, ram text, monitor text, keyboard text, mouse text, phone_charge text, bool_smokingArea boolean, bool_card boolean, bool_parkinglot boolean, bool_office boolean, bool_print boolean, bool_scanner boolean, printPrice text, colorPrintPrice text, scannerPrice text )";
        sqLiteDatabase.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String sql = " DROP TABLE PCROOM ";
        sqLiteDatabase.execSQL(sql);
        onCreate(sqLiteDatabase);
    }
}

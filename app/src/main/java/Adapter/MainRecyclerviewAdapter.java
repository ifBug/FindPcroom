package Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.findpcroomkss.DetailActivity;
import com.example.findpcroomkss.R;

import java.util.ArrayList;

import Data.PcroomData;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainRecyclerviewAdapter extends RecyclerView.Adapter<MainRecyclerviewAdapter.ViewHolder> {

    Context context;
    ArrayList<PcroomData> datas;

    public MainRecyclerviewAdapter(Context context, ArrayList<PcroomData> datas) {
        this.context = context;
        this.datas = datas;
    }

    @NonNull
    @Override
    public MainRecyclerviewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_main_rv, viewGroup, false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MainRecyclerviewAdapter.ViewHolder viewHolder, int i) {
        PcroomData currentData = datas.get(i);
        Glide.with(context).load(currentData.getImageUrl()).thumbnail(0.1f).into(viewHolder.preview);
        viewHolder.distance.setText(currentData.getDistance() + "km");
        viewHolder.name.setText(currentData.getName());
        viewHolder.reservation.setText("(" + currentData.getReservationAvailable() + "명 가능)");
        viewHolder.price.setText(currentData.getPrice());
        viewHolder.CPU.setText(currentData.getCpu());
        viewHolder.GPU.setText(currentData.getGpu());

    }

    @Override
    public int getItemCount() {
        return datas == null ? 0 : datas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_main_rv_fl_root)
        FrameLayout root;
        @BindView(R.id.item_main_rv_iv_preview)
        ImageView preview;
        @BindView(R.id.item_main_rv_ll_viewmap)
        LinearLayout viewmap;
        @BindView(R.id.item_main_rv_tv_distance)
        TextView distance;
        @BindView(R.id.item_main_rv_tv_name)
        TextView name;
        @BindView(R.id.item_main_rv_tv_reservation)
        TextView reservation;
        @BindView(R.id.item_main_rv_tv_price)
        TextView price;
        @BindView(R.id.item_main_rv_tv_CPU)
        TextView CPU;
        @BindView(R.id.item_main_rv_tv_GPU)
        TextView GPU;

        @OnClick(R.id.item_main_rv_fl_root)
        public void OnClick(View v) {
            Intent intent = new Intent(context, DetailActivity.class);
            //datas에 클릭한 번호에 있는 키를 넘겨주겠다.(위치 저장을 key로 하겠다.)
            intent.putExtra("key", datas.get(getAdapterPosition()).getKey());
            context.startActivity(intent);      //start activity를 하고 intent대로 한다?


//            Toast.makeText(context, getAdapterPosition()+"", Toast.LENGTH_SHORT).show();
//            getAdapterPosition();
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

package Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.findpcroomkss.R;
import com.github.demono.adapter.InfinitePagerAdapter;

import java.util.ArrayList;

public class DetailViewPagerAdapter extends InfinitePagerAdapter {

    Context context;
    ArrayList<String> datas = new ArrayList<>();
    LayoutInflater mInflater;

    public DetailViewPagerAdapter(Context context, ArrayList<String> datas) {
        this.context = context;
        this.datas = datas;
        mInflater= (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getItemCount() {
        return datas == null ? 0 : datas.size();
    }

    @Override
    public View getItemView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if(view == null) {
            view = mInflater.inflate(R.layout.adapter_detail_viewpager, viewGroup, false);
            holder = new ViewHolder();
            holder.iv = (ImageView)view.findViewById(R.id.item_iv);
            view.setTag(holder);
        } else {
            holder = (ViewHolder)view.getTag();
        }

        Glide.with(context).load(datas.get(i)).thumbnail(0.1f).into(holder.iv);
        return view;
    }

    private static class ViewHolder {
        ImageView iv;
    }
}
